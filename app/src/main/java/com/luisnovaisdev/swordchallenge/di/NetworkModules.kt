package com.luisnovaisdev.swordchallenge.di

import com.luisnovaisdev.swordchallenge.core.network.RetrofitClient
import com.luisnovaisdev.swordchallenge.feature_catbreads.data.datasources.remote.services.CatBreedsServiceInterface
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModules {

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit {
        return RetrofitClient.retrofit
    }

    @Provides
    @Singleton
    fun provideCatBreedsServiceInterface(
        retrofit: Retrofit
    ): CatBreedsServiceInterface{
        return retrofit.create(CatBreedsServiceInterface::class.java)
    }

}