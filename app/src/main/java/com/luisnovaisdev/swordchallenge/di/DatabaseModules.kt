package com.luisnovaisdev.swordchallenge.di

import android.content.Context
import androidx.room.Room
import com.luisnovaisdev.swordchallenge.core.constants.DatabaseConstants
import com.luisnovaisdev.swordchallenge.core.database.SwordDatabase
import com.luisnovaisdev.swordchallenge.feature_catbreads.data.datasources.local.dao.CatBreedDAO
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModules {

    @Singleton
    @Provides
    fun provideSwordDatabase(
        @ApplicationContext app: Context
    ): SwordDatabase {
        return Room.databaseBuilder(
            app,
            SwordDatabase::class.java,
            DatabaseConstants.SWORD_DATABASE
        )
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideCatBreedDao(swordDatabase: SwordDatabase) = swordDatabase.catBreedDao()

}