package com.luisnovaisdev.swordchallenge.di

import com.luisnovaisdev.swordchallenge.feature_catbreads.data.repository.CatBreedRepositoryImpl
import com.luisnovaisdev.swordchallenge.feature_catbreads.domain.repository.CatBreedRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModules {

    @Singleton
    @Provides
    fun provideCatBreedsRepository(
        catBreedRepositoryImpl: CatBreedRepositoryImpl
    ): CatBreedRepository {
        return catBreedRepositoryImpl
    }


}