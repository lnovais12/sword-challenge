package com.luisnovaisdev.swordchallenge.ui.models

import androidx.annotation.StringRes
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Home
import androidx.compose.ui.graphics.vector.ImageVector
import com.luisnovaisdev.swordchallenge.R

sealed class Screen(val route: String, val icon: ImageVector? = null, @StringRes val resourceId: Int? = null) {
    object CatBreeds : Screen("cat_breeds", Icons.Default.Home, R.string.cat_breeds)
    object CatBreedsFavourites : Screen("cat_breeds_favourites", Icons.Default.Favorite, R.string.favourites)
    object CatBreedDetail : Screen("cat_breed_detail/{catBreedId}")  {

        fun buildRoute(catBreedId: String): String {
           return "cat_breed_detail/$catBreedId"
        }

    }
}
