package com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.luisnovaisdev.swordchallenge.core.utils.ResponseError
import com.luisnovaisdev.swordchallenge.core.utils.ResponseSuccess
import com.luisnovaisdev.swordchallenge.di.IoDispatcher
import com.luisnovaisdev.swordchallenge.feature_catbreads.domain.repository.CatBreedRepository
import com.luisnovaisdev.swordchallenge.feature_catbreads.domain.usecases.HandleCatBreedFavouriteUseCase
import com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.uistates.CatBreedsUiState
import com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.uistates.FavouritesCatBreedsUiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavouritesCatBreedsScreenViewModel @Inject constructor(
    private val catBreedRepository: CatBreedRepository,
    private val handleCatBreedFavourite: HandleCatBreedFavouriteUseCase,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : ViewModel() {

    private val _uiState = MutableStateFlow(FavouritesCatBreedsUiState())
    val state = _uiState.asStateFlow()

    init {
        getFavouritesCatBreeds()
    }

    private fun getFavouritesCatBreeds() {
        _uiState.update {
            it.copy(
                isLoading = true
            )
        }
        viewModelScope.launch {
            catBreedRepository.getFavouriteCatBreeds()
                .flowOn(
                    ioDispatcher
                ).catch {
                    _uiState.update {
                        it.copy(
                            isLoading = false,
                        )
                    }
                }.collectLatest { catBreeds ->
                    if (catBreeds is ResponseSuccess) {
                        _uiState.update {
                            it.copy(
                                isLoading = false,
                                catBreeds = catBreeds.data
                            )
                        }
                    }
                }
        }
    }

    fun handleFavouriteClick(catBreedId: String, isFavourite: Boolean) {
        viewModelScope.launch {
            handleCatBreedFavourite(catBreedId, isFavourite)
        }
    }

}