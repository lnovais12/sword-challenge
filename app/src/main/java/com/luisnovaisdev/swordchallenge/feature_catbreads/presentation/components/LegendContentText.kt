package com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.components

import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha

@Composable
fun LegendContentText(legend: String, content: String){

    Column {
        Text(text = legend, Modifier.alpha(0.5f))
        Text(text = content)
    }

}