package com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.luisnovaisdev.swordchallenge.feature_catbreads.domain.models.CatBreed

@Composable
fun CatBreedItem(
    catBreed: CatBreed,
    handleFavouriteClick: (catBreedId: String, isFavourite: Boolean) -> Unit,
    openCatBreedDetail: (catBreadId: String) -> Unit
){
    Box{
        Column(
            modifier = Modifier.clickable {
                openCatBreedDetail(catBreed.id)
            }
        ) {
            AsyncImage(
                model = catBreed.imageUrl,
                contentDescription = null,
                modifier = Modifier.height(150.dp),
                contentScale = ContentScale.Crop
            )
            Text(
                text = catBreed.name,
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Center
            )
        }
        FavouriteIconButton(
            catBreedId = catBreed.id,
            isFavourite = catBreed.isFavourite,
            handleFavouriteClick = handleFavouriteClick,
            modifier = Modifier.align(
                Alignment.TopEnd
            )
        )
    }
}