package com.luisnovaisdev.swordchallenge.feature_catbreads.domain.usecases

import com.luisnovaisdev.swordchallenge.feature_catbreads.domain.models.CatBreed
import javax.inject.Inject

class SearchCatBreedUseCase @Inject constructor(

) {

    operator fun invoke(stringToSearch: String, catBreeds: List<CatBreed>): List<CatBreed> {
        return catBreeds.filter {
            it.name.contains(stringToSearch, ignoreCase = true)
        }
    }

}