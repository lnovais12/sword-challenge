package com.luisnovaisdev.swordchallenge.feature_catbreads.domain.models

data class CatBreed(
    val id: String,
    val name: String,
    val description: String,
    val origin: String,
    val temperament: String,
    val referenceImageId: String? = null,
    val imageUrl: String? = null,
    val isFavourite: Boolean = false,
)
