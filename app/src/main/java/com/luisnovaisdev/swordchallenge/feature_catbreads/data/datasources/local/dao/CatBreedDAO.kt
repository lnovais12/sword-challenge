package com.luisnovaisdev.swordchallenge.feature_catbreads.data.datasources.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.luisnovaisdev.swordchallenge.feature_catbreads.data.datasources.local.models.CatBreedEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface CatBreedDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertCatBreeds(catBreeds: CatBreedEntity)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertCatBreeds(catBreeds: List<CatBreedEntity>)

    @Query("SELECT * FROM cat_breeds ORDER BY name ASC")
    fun getAllCatBreeds(): Flow<List<CatBreedEntity>>

    @Query("SELECT * FROM cat_breeds WHERE id = :catBreedId")
    fun getCatBreed(catBreedId: String): Flow<CatBreedEntity?>

    @Query("SELECT * FROM cat_breeds WHERE isFavourite = 1")
    fun getFavouritesCatBreeds(): Flow<List<CatBreedEntity>>

    @Query("UPDATE cat_breeds SET isFavourite = 1 WHERE id = :catBreedId")
    suspend fun markCatBreedAsFavourite(catBreedId: String)

    @Query("UPDATE cat_breeds SET isFavourite = 0 WHERE id = :catBreedId")
    suspend fun removeCatBreedAsFavourite(catBreedId: String)
}