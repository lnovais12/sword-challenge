package com.luisnovaisdev.swordchallenge.feature_catbreads.data.datasources.remote.models

import com.google.gson.annotations.SerializedName

data class CatBreedDTO(
    val id: String,
    val description: String?,
    val name: String?,
    val origin: String?,
    val temperament: String?,
    @SerializedName("reference_image_id")
    val referenceImageId: String?,
    val image: CatBreedImageDTO?,
)