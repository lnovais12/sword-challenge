package com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.components

import androidx.compose.material3.SnackbarHostState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.screens.CatBreedDetailScreen
import com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.screens.CatBreedsScreen
import com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.screens.FavouritesCatBreedsScreen
import com.luisnovaisdev.swordchallenge.ui.models.Screen

@Composable
fun NavigationHost(navController: NavHostController, snackbarHostState: SnackbarHostState, modifier: Modifier = Modifier) {
    NavHost(navController, startDestination = Screen.CatBreeds.route, modifier) {
        composable(Screen.CatBreeds.route) {
            CatBreedsScreen(navController,snackbarHostState)
        }
        composable(Screen.CatBreedsFavourites.route) {
            FavouritesCatBreedsScreen(navController)
        }
        composable(Screen.CatBreedDetail.route) { backStackEntry ->
            val catBreedId = backStackEntry.arguments?.getString("catBreedId")
            catBreedId?.let {
                CatBreedDetailScreen(catBreedId)
            }
        }
    }
}