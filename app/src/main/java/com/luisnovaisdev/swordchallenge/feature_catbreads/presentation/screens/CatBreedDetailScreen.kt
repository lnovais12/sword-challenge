package com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.screens

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.AsyncImage
import com.luisnovaisdev.swordchallenge.R
import com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.components.FavouriteIconButton
import com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.components.LegendContentText
import com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.components.PageTitle
import com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.viewmodels.CatBreedDetailScreenViewModel

@Composable
fun CatBreedDetailScreen(catBreedId: String) {

    val viewModel: CatBreedDetailScreenViewModel = hiltViewModel()

    val uiState = viewModel.state.collectAsState().value

    LaunchedEffect(Unit){
        viewModel.getCatBreedDetail(catBreedId)
    }

    Column(modifier = Modifier.padding(PaddingValues(20.dp, 0.dp)).verticalScroll(rememberScrollState())) {
        PageTitle(title = uiState.catBreed?.name ?: stringResource(id = R.string.cat_breed_not_found))

        uiState.catBreed?.let { catBreed ->
            Box {
                AsyncImage(
                    model = catBreed.imageUrl,
                    contentDescription = null,
                    modifier = Modifier.height(300.dp),
                    contentScale = ContentScale.Crop
                )
                FavouriteIconButton(
                    catBreedId = catBreed.id,
                    isFavourite = catBreed.isFavourite,
                    handleFavouriteClick = { catBreedId, isFavourite ->
                        viewModel.handleFavouriteClick(
                            catBreedId,
                            isFavourite
                        )
                    },
                    modifier = Modifier.align(
                        Alignment.TopEnd
                    )
                )
            }

            Spacer(modifier = Modifier.height(20.dp))

            Text(text = catBreed.description)

            Spacer(modifier = Modifier.height(20.dp))

            LegendContentText(
                legend = stringResource(id = R.string.origin_legend),
                content = catBreed.origin
            )

            Spacer(modifier = Modifier.height(10.dp))

            LegendContentText(
                legend = stringResource(id = R.string.temperature_legend),
                content = catBreed.temperament
            )

            Spacer(modifier = Modifier.height(20.dp))
        }


    }

}