package com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.screens

import android.widget.ProgressBar
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.FavoriteBorder
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FilledIconButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.hilt.navigation.compose.hiltViewModel
import com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.viewmodels.CatBreedsScreenViewModel
import androidx.compose.material3.ListItem
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.SearchBar
import androidx.compose.material3.SnackbarHostState
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import coil.compose.AsyncImage
import com.luisnovaisdev.swordchallenge.R
import com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.components.CatBreedItem
import com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.components.PageTitle
import com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.viewmodels.FavouritesCatBreedsScreenViewModel
import com.luisnovaisdev.swordchallenge.ui.models.Screen
import kotlinx.coroutines.launch


@Composable
fun FavouritesCatBreedsScreen(navController: NavController) {

    val viewModel: FavouritesCatBreedsScreenViewModel = hiltViewModel()

    val uiState = viewModel.state.collectAsState().value

    Column(modifier = Modifier.padding(PaddingValues(20.dp, 0.dp))) {
        PageTitle(title = stringResource(id = R.string.favourites))
        
        if(uiState.isLoading){
            Box(modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()) {
                CircularProgressIndicator(
                    modifier = Modifier
                        .width(64.dp)
                        .align(Alignment.Center),
                    color = MaterialTheme.colorScheme.secondary,
                    trackColor = MaterialTheme.colorScheme.surfaceVariant,
                )
            }

            return
        }

        Spacer(modifier = Modifier.height(20.dp))

        LazyVerticalGrid(
            columns = GridCells.Fixed(2)
        ) {
            items(uiState.catBreeds, key = {it.id}){ catBreed ->
                ListItem(
                    headlineContent = {
                        CatBreedItem(
                            catBreed = catBreed,
                            handleFavouriteClick = { catBreedId, isFavourite ->
                                viewModel.handleFavouriteClick(
                                    catBreedId, isFavourite
                                )
                            },
                            openCatBreedDetail = { catBreedId ->
                                navController.navigate(Screen.CatBreedDetail.buildRoute(catBreedId))
                            }
                        )
                    }
                )
            }
        }
    }
}