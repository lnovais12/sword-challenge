package com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun PageTitle(title: String){
    Column {
        Spacer(modifier = Modifier.height(50.dp))
        Text(title, fontSize = 30.sp)
        Spacer(modifier = Modifier.height(20.dp))
    }

}