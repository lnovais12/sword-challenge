package com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.screens

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.hilt.navigation.compose.hiltViewModel
import com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.viewmodels.CatBreedsScreenViewModel
import androidx.compose.material3.ListItem
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.SearchBar
import androidx.compose.material3.SnackbarHostState
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.luisnovaisdev.swordchallenge.R
import com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.components.CatBreedItem
import com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.components.PageTitle
import com.luisnovaisdev.swordchallenge.ui.models.Screen


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CatBreedsScreen(navController: NavController, snackbarHostState: SnackbarHostState) {

    val viewModel: CatBreedsScreenViewModel = hiltViewModel()

    val uiState = viewModel.state.collectAsState().value

    if(!uiState.errorMessage.isNullOrBlank()){
        LaunchedEffect(Unit){
            snackbarHostState.showSnackbar(uiState.errorMessage)
        }
    }

    Column(modifier = Modifier.padding(PaddingValues(20.dp, 0.dp))) {
        PageTitle(title = stringResource(id = R.string.cat_breeds))

        if(uiState.isLoading){
            Box(modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()) {
                CircularProgressIndicator(
                    modifier = Modifier
                        .width(64.dp)
                        .align(Alignment.Center),
                    color = MaterialTheme.colorScheme.secondary,
                    trackColor = MaterialTheme.colorScheme.surfaceVariant,
                )
            }

            return
        }

        SearchBar(
            query = uiState.searchString ?: "",
            onQueryChange = {viewModel.updateSearchQuery(it)},
            onSearch = {viewModel.updateSearchQuery(it)},
            active = false,
            onActiveChange = {},
            leadingIcon = {
                Icon(Icons.Filled.Search, contentDescription = "")
            },
            trailingIcon = {
               if(!uiState.searchString.isNullOrBlank()){
                   IconButton(onClick = { viewModel.updateSearchQuery("") }) {
                       Icon(Icons.Filled.Clear, contentDescription = "")
                   }
               }
            },
            placeholder = {
                Text(stringResource(id = R.string.search_placeholder))
            },
            modifier = Modifier.fillMaxWidth()
        ) { }

        Spacer(modifier = Modifier.height(20.dp))

        LazyVerticalGrid(
            columns = GridCells.Fixed(2)
        ) {
            val items = if(uiState.searchString.isNullOrBlank()){
                uiState.catBreeds
            }else{
                uiState.filteredCatBreeds
            }
            items(items, key = {it.id}){ catBreed ->
                ListItem(
                    headlineContent = {
                        CatBreedItem(
                            catBreed = catBreed,
                            handleFavouriteClick = { catBreedId, isFavourite ->
                                viewModel.handleFavouriteClick(
                                    catBreedId, isFavourite
                                )
                            },
                            openCatBreedDetail = { catBreedId ->
                                navController.navigate(Screen.CatBreedDetail.buildRoute(catBreedId))
                            }
                        )
                    }
                )
            }
        }
    }
}