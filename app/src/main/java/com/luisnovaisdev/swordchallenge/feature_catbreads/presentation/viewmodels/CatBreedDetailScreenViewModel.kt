package com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.luisnovaisdev.swordchallenge.core.utils.ResponseError
import com.luisnovaisdev.swordchallenge.core.utils.ResponseSuccess
import com.luisnovaisdev.swordchallenge.di.IoDispatcher
import com.luisnovaisdev.swordchallenge.feature_catbreads.domain.repository.CatBreedRepository
import com.luisnovaisdev.swordchallenge.feature_catbreads.domain.usecases.HandleCatBreedFavouriteUseCase
import com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.uistates.CatBreedDetailUiState
import com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.uistates.CatBreedsUiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CatBreedDetailScreenViewModel @Inject constructor(
    private val catBreedsRepository: CatBreedRepository,
    private val handleCatBreedFavourite: HandleCatBreedFavouriteUseCase,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
) : ViewModel() {

    private val _uiState = MutableStateFlow(CatBreedDetailUiState())
    val state = _uiState.asStateFlow()

    fun getCatBreedDetail(catBreedId: String?) {
        _uiState.update {
            it.copy(
                isLoading = true
            )
        }
        viewModelScope.launch {
            catBreedId?.let {
                catBreedsRepository.getCatBreed(catBreedId)
                    .flowOn(ioDispatcher)
                    .collectLatest { catBreedResponse ->
                        when (catBreedResponse) {
                            is ResponseSuccess -> {
                                _uiState.update {
                                    it.copy(
                                        isLoading = false,
                                        catBreed = catBreedResponse.data
                                    )
                                }
                            }

                            is ResponseError -> {
                                _uiState.update {
                                    it.copy(
                                        isLoading = false,
                                        catBreed = null
                                    )
                                }
                            }
                        }
                    }
            }

        }
    }

    fun handleFavouriteClick(catBreedId: String, isFavourite: Boolean) {
        viewModelScope.launch {
            handleCatBreedFavourite(catBreedId, isFavourite)
        }
    }

}