package com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.uistates

import com.luisnovaisdev.swordchallenge.feature_catbreads.domain.models.CatBreed

data class CatBreedDetailUiState(
    val isLoading: Boolean = false,
    val catBreed: CatBreed? = null,
)
