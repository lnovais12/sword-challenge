package com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.luisnovaisdev.swordchallenge.core.utils.ResponseError
import com.luisnovaisdev.swordchallenge.core.utils.ResponseSuccess
import com.luisnovaisdev.swordchallenge.di.IoDispatcher
import com.luisnovaisdev.swordchallenge.feature_catbreads.domain.repository.CatBreedRepository
import com.luisnovaisdev.swordchallenge.feature_catbreads.domain.usecases.HandleCatBreedFavouriteUseCase
import com.luisnovaisdev.swordchallenge.feature_catbreads.domain.usecases.SearchCatBreedUseCase
import com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.uistates.CatBreedsUiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CatBreedsScreenViewModel @Inject constructor(
    private val catBreedRepository: CatBreedRepository,
    private val handleCatBreedFavourite: HandleCatBreedFavouriteUseCase,
    private val searchCatBreed: SearchCatBreedUseCase,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
) : ViewModel() {

    private val _uiState = MutableStateFlow(CatBreedsUiState())
    val state = _uiState.asStateFlow()

    init {
        getCatBreeds()
    }

    private fun getCatBreeds() {
        _uiState.update {
            it.copy(
                isLoading = true
            )
        }
        viewModelScope.launch {
            catBreedRepository.getCatBreeds()
                .flowOn(
                    ioDispatcher
                ).catch {
                    _uiState.update {
                        it.copy(
                            isLoading = false,
                            errorMessage = it.errorMessage
                        )
                    }
                }.collectLatest { catBreeds ->
                    if (catBreeds is ResponseSuccess) {
                        _uiState.update {
                            it.copy(
                                isLoading = false,
                                catBreeds = catBreeds.data
                            )
                        }
                    }
                    if (catBreeds is ResponseError) {
                        _uiState.update {
                            it.copy(
                                isLoading = false,
                                errorMessage = catBreeds.message
                            )
                        }
                    }
                }
        }

    }

    fun updateSearchQuery(searchQuery: String) {
        val filteredCatBreeds = searchCatBreed(
            stringToSearch = searchQuery,
            catBreeds = _uiState.value.catBreeds
        )
        _uiState.update {
            it.copy(
                searchString = searchQuery,
                filteredCatBreeds = filteredCatBreeds,
            )
        }
    }

    fun handleFavouriteClick(catBreedId: String, isFavourite: Boolean) {
        viewModelScope.launch {
            handleCatBreedFavourite(catBreedId, isFavourite)
        }
    }

}