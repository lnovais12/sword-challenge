package com.luisnovaisdev.swordchallenge.feature_catbreads.data.datasources.remote.services

import com.luisnovaisdev.swordchallenge.feature_catbreads.data.datasources.remote.models.CatBreedDTO
import retrofit2.Response
import retrofit2.http.GET

interface CatBreedsServiceInterface {

    @GET("breeds")
    suspend fun getCatBreeds(): Response<List<CatBreedDTO>>

}