package com.luisnovaisdev.swordchallenge.feature_catbreads.data.repository

import com.luisnovaisdev.swordchallenge.core.network.ApiCallHandler
import com.luisnovaisdev.swordchallenge.core.network.ApiError
import com.luisnovaisdev.swordchallenge.core.network.ApiException
import com.luisnovaisdev.swordchallenge.core.network.ApiSuccess
import com.luisnovaisdev.swordchallenge.core.utils.RepositoryResponse
import com.luisnovaisdev.swordchallenge.core.utils.ResponseError
import com.luisnovaisdev.swordchallenge.core.utils.ResponseSuccess
import com.luisnovaisdev.swordchallenge.feature_catbreads.data.datasources.local.dao.CatBreedDAO
import com.luisnovaisdev.swordchallenge.feature_catbreads.data.datasources.remote.services.CatBreedsServiceInterface
import com.luisnovaisdev.swordchallenge.feature_catbreads.data.mappers.toCatBreed
import com.luisnovaisdev.swordchallenge.feature_catbreads.data.mappers.toCatBreedEntity
import com.luisnovaisdev.swordchallenge.feature_catbreads.domain.models.CatBreed
import com.luisnovaisdev.swordchallenge.feature_catbreads.domain.repository.CatBreedRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class CatBreedRepositoryImpl @Inject constructor(
    private val apiCallHandler: ApiCallHandler,
    private val catBreedsServiceInterface: CatBreedsServiceInterface,
    private val catBreedDao: CatBreedDAO
): CatBreedRepository {

    override fun getCatBreeds(): Flow<RepositoryResponse<List<CatBreed>>> {

        return flow {
            emitAll(
                catBreedDao.getAllCatBreeds().map { catBreeds ->
                    if(catBreeds.isEmpty()){
                        emit(getRemoteCatBreeds())
                    }
                    ResponseSuccess(
                        data = catBreeds.map { it.toCatBreed() }
                    )
                }
            )
        }
    }

    override suspend fun getCatBreed(catBreedId: String): Flow<RepositoryResponse<CatBreed>> {
        return catBreedDao.getCatBreed(catBreedId).map { catBreedEntity ->
            catBreedEntity?.let {
                ResponseSuccess(it.toCatBreed())
            } ?: run {
                ResponseError(message = ResponseError.NOT_FOUND_ERROR)
            }
        }
    }

    override fun getFavouriteCatBreeds(): Flow<RepositoryResponse<List<CatBreed>>> {
        return catBreedDao.getFavouritesCatBreeds().map { catBreeds ->
            ResponseSuccess(
                data = catBreeds.map { it.toCatBreed() }
            )
        }
    }

    override suspend fun markCatBreedAsFavourite(catBreedId: String) {
        catBreedDao.markCatBreedAsFavourite(catBreedId)
    }

    override suspend fun removeCatBreedAsFavourite(catBreedId: String) {
        catBreedDao.removeCatBreedAsFavourite(catBreedId)
    }

    private suspend fun getRemoteCatBreeds(): RepositoryResponse<List<CatBreed>> {
        val response = apiCallHandler.handleApi {
            catBreedsServiceInterface.getCatBreeds()
        }

        return when(response){
            is ApiError -> {
                ResponseError(
                    message = response.message,
                )
            }
            is ApiException -> {
                ResponseError(
                    message = response.e.localizedMessage,
                )
            }
            is ApiSuccess -> {
                val catBreedsEntities = response.data.map {
                    it.toCatBreedEntity()
                }
                val catBreeds = catBreedsEntities.map {
                    it.toCatBreed()
                }
                catBreedDao.insertCatBreeds(catBreedsEntities)
                ResponseSuccess(data = catBreeds)
            }
        }
    }
}