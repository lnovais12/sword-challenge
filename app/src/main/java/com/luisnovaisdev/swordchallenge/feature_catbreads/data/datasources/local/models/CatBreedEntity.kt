package com.luisnovaisdev.swordchallenge.feature_catbreads.data.datasources.local.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cat_breeds")
data class CatBreedEntity(
    @PrimaryKey
    val id: String,
    val name: String,
    val description: String,
    val origin: String,
    val temperament: String,
    val referenceImageId: String? = null,
    val imageUrl: String? = null,
    val isFavourite: Boolean = false,
)