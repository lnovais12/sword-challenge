package com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.components

import androidx.compose.material3.Icon
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import androidx.navigation.compose.currentBackStackEntryAsState
import com.luisnovaisdev.swordchallenge.ui.models.Screen

@Composable
fun BottomNavigationBar(navController: NavController) {
    NavigationBar  {
        val navBackStackEntry by navController.currentBackStackEntryAsState()
        val currentRoute = navBackStackEntry?.destination?.route

        val bottomNavigationItems = listOf(
            Screen.CatBreeds,
            Screen.CatBreedsFavourites
        )

        bottomNavigationItems.forEach { item ->
            NavigationBarItem(
                selected = currentRoute == item.route,
                onClick = {
                    navController.navigate(item.route) {
                        popUpTo(navController.graph.startDestinationId)
                        launchSingleTop = true
                    }
                },
                icon = {
                    item.icon?.let {
                        Icon(item.icon, contentDescription = null)
                    }
                },
                label = {
                    item.resourceId?.let {
                        Text(stringResource(id = item.resourceId))
                    }
                }
            )
        }
    }
}