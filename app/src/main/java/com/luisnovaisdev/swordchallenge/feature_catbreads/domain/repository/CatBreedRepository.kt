package com.luisnovaisdev.swordchallenge.feature_catbreads.domain.repository

import com.luisnovaisdev.swordchallenge.core.utils.RepositoryResponse
import com.luisnovaisdev.swordchallenge.feature_catbreads.domain.models.CatBreed
import kotlinx.coroutines.flow.Flow

interface CatBreedRepository {

    fun getCatBreeds(): Flow<RepositoryResponse<List<CatBreed>>>

    fun getFavouriteCatBreeds(): Flow<RepositoryResponse<List<CatBreed>>>

    suspend fun getCatBreed(catBreedId: String): Flow<RepositoryResponse<CatBreed>>

    suspend fun markCatBreedAsFavourite(catBreedId: String)

    suspend fun removeCatBreedAsFavourite(catBreedId: String)
}