package com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.uistates

import com.luisnovaisdev.swordchallenge.feature_catbreads.domain.models.CatBreed

data class CatBreedsUiState(
    val isLoading: Boolean = false,
    val catBreeds: List<CatBreed> = emptyList(),
    val filteredCatBreeds: List<CatBreed> = emptyList(),
    val searchString: String? = null,
    val errorMessage: String? = null,
)
