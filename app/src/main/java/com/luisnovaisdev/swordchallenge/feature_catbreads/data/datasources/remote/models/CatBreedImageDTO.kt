package com.luisnovaisdev.swordchallenge.feature_catbreads.data.datasources.remote.models

data class CatBreedImageDTO(
    val id: String,
    val url: String,
)
