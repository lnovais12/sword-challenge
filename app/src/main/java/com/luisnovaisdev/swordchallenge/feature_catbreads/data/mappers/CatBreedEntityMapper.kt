package com.luisnovaisdev.swordchallenge.feature_catbreads.data.mappers

import com.luisnovaisdev.swordchallenge.feature_catbreads.data.datasources.local.models.CatBreedEntity
import com.luisnovaisdev.swordchallenge.feature_catbreads.data.datasources.remote.models.CatBreedDTO
import com.luisnovaisdev.swordchallenge.feature_catbreads.domain.models.CatBreed

fun CatBreedEntity.toCatBreed(): CatBreed {
    return CatBreed(
        id = id,
        name = name,
        description = description,
        origin = origin,
        temperament = temperament,
        referenceImageId = referenceImageId,
        imageUrl = imageUrl,
        isFavourite = isFavourite
    )
}

fun CatBreedDTO.toCatBreedEntity(): CatBreedEntity {
    return CatBreedEntity(
        id = id,
        name = name ?: "-",
        description = description ?: "-",
        origin = origin ?: "-",
        temperament = temperament ?: "-",
        referenceImageId = referenceImageId,
        imageUrl = image?.url
    )
}