package com.luisnovaisdev.swordchallenge.feature_catbreads.domain.usecases

import com.luisnovaisdev.swordchallenge.feature_catbreads.domain.repository.CatBreedRepository
import javax.inject.Inject

class HandleCatBreedFavouriteUseCase @Inject constructor(
    private val catBreedRepository: CatBreedRepository
) {

    suspend operator fun invoke(catBreedId: String, isFavourite: Boolean){
        if (isFavourite) {
            catBreedRepository.removeCatBreedAsFavourite(catBreedId)
            return
        }

        catBreedRepository.markCatBreedAsFavourite(catBreedId)
    }

}