package com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.components

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.FavoriteBorder
import androidx.compose.material3.FilledIconButton
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@Composable
fun FavouriteIconButton(
    catBreedId: String,
    isFavourite: Boolean,
    handleFavouriteClick: (catBreedId: String, isFavourite: Boolean) -> Unit,
    modifier: Modifier
) {

    val favouriteIcon = if(isFavourite){
        Icons.Filled.Favorite
    }else{
        Icons.Filled.FavoriteBorder
    }
    FilledIconButton(
        modifier = modifier,
        content= {
            Icon(favouriteIcon, contentDescription = "")
        },
        onClick = {
            handleFavouriteClick(
                catBreedId,
                isFavourite
            )
        }
    )

}