package com.luisnovaisdev.swordchallenge.core.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.luisnovaisdev.swordchallenge.feature_catbreads.data.datasources.local.dao.CatBreedDAO
import com.luisnovaisdev.swordchallenge.feature_catbreads.data.datasources.local.models.CatBreedEntity

@Database(
    entities = [
        CatBreedEntity::class
    ],
    version = 1,
    exportSchema = false,
)
abstract class SwordDatabase : RoomDatabase() {

    abstract fun catBreedDao(): CatBreedDAO

}