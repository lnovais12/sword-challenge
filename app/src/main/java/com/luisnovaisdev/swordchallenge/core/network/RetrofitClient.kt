package com.luisnovaisdev.swordchallenge.core.network

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {

    private const val BASE_URL = "https://api.thecatapi.com/v1/"
    private const val API_KEY = "live_a4KRZgcHglAc2MmpVnSWRvZBi5Am0Nz0DkPi6xNTXXrpPP7aeX1GQUsE7Jo6aN0u"

    val retrofit: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val okHttpClient: OkHttpClient by lazy {
        OkHttpClient.Builder().apply {
            addInterceptor(
                Interceptor { chain ->
                    val builder = chain.request().newBuilder()
                    builder.header("x-api-key", API_KEY)
                    return@Interceptor chain.proceed(builder.build())
                }
            )
        }.build()
    }

}