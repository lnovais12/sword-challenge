package com.luisnovaisdev.swordchallenge.core.constants

object DatabaseConstants {

    const val SWORD_DATABASE = "sword"
    const val CAT_BREED_TABLE = "cat_breeds"

}