package com.luisnovaisdev.swordchallenge.core.utils

import com.luisnovaisdev.swordchallenge.core.network.ApiResult

sealed interface RepositoryResponse<T : Any>

class ResponseSuccess<T : Any>(val data: T) : RepositoryResponse<T>

class ResponseError<T : Any>(val message: String?, val data: T? = null) : RepositoryResponse<T> {

    companion object {

        val NOT_FOUND_ERROR = "not_found"

    }

}