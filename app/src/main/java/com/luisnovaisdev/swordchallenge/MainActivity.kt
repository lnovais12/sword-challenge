package com.luisnovaisdev.swordchallenge

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Warning
import androidx.compose.material3.ExtendedFloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.components.BottomNavigationBar
import com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.components.NavigationHost
import com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.screens.CatBreedDetailScreen
import com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.screens.CatBreedsScreen
import com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.screens.FavouritesCatBreedsScreen
import com.luisnovaisdev.swordchallenge.feature_catbreads.presentation.viewmodels.CatBreedsScreenViewModel
import com.luisnovaisdev.swordchallenge.ui.models.Screen
import com.luisnovaisdev.swordchallenge.ui.theme.SwordChallengeTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            SwordChallengeTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val navController = rememberNavController()
                    val snackbarHostState = remember { SnackbarHostState() }

                    Scaffold(
                        bottomBar = {
                            val navBackStackEntry by navController.currentBackStackEntryAsState()
                            val currentRoute = navBackStackEntry?.destination?.route
                            val isNotCatBreedDetail = currentRoute != Screen.CatBreedDetail.route
                            if(isNotCatBreedDetail){
                                BottomNavigationBar(navController = navController)
                            }
                        },
                        snackbarHost = {
                            SnackbarHost(hostState = snackbarHostState)
                        },
                    ) { innerPadding ->
                        NavigationHost(
                            navController = navController,
                            snackbarHostState = snackbarHostState,
                            modifier = Modifier.padding(innerPadding)
                        )
                    }
                }
            }
        }
    }
}



