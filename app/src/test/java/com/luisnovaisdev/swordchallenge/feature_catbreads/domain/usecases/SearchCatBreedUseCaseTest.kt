package com.luisnovaisdev.swordchallenge.feature_catbreads.domain.usecases

import com.luisnovaisdev.swordchallenge.feature_catbreads.domain.models.CatBreed
import org.junit.Test

class SearchCatBreedUseCaseTest {

    @Test
    fun searchShouldIgnoreCamelCase() {

        val catBreeds = listOf(
            CatBreed(
                id = "CatBreed1",
                name = "Cat Breed 1",
                description = "",
                origin = "",
                temperament = "",
            ),
            CatBreed(
                id = "CatBreed2",
                name = "cat Breed 2",
                description = "",
                origin = "",
                temperament = "",
            ),
            CatBreed(
                id = "CatBreed3",
                name = "Breed 2.1",
                description = "",
                origin = "",
                temperament = "",
            )
        )
        val stringToSearch = "cat"

        val filteredCatBreeds = SearchCatBreedUseCase().invoke(
            stringToSearch = stringToSearch,
            catBreeds = catBreeds
        )

        assert(filteredCatBreeds.size == 2)
    }

    @Test
    fun searchShouldFilterItemsContainsSearchString() {

        val catBreeds = listOf(
            CatBreed(
                id = "CatBreed1",
                name = "Cat Breed 1",
                description = "",
                origin = "",
                temperament = "",
            ),
            CatBreed(
                id = "CatBreed2",
                name = "cat Breed 2",
                description = "",
                origin = "",
                temperament = "",
            ),
            CatBreed(
                id = "CatBreed3",
                name = "Breed 2.1",
                description = "",
                origin = "",
                temperament = "",
            )
        )
        val stringToSearch = "Breed 2."

        val filteredCatBreeds = SearchCatBreedUseCase().invoke(
            stringToSearch = stringToSearch,
            catBreeds = catBreeds
        )

        assert(filteredCatBreeds.size == 1 && filteredCatBreeds[0].id == "CatBreed3")
    }

}